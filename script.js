//Variables
var drawing = false;

var selectedTool = null;

var selectedColor = "black";

var penOpacity = 1;
var penSize = document.getElementById('sizeSlid').value;

var textSize = 100;
var textFont = 'Arial';

var canvas = document.getElementById('myCanvas');
var previewCanvas = document.getElementById('previewCanvas');

var oriCanvasState
var oriImage = new Image();

var ctx = canvas.getContext('2d');
var ctx2 = previewCanvas.getContext('2d');
//----------------------------------------------------------------------
// Canvas draw/erase

canvas.style.position = 'fixed';
previewCanvas.style.display = 'none';

resize(canvas.scrollWidth, canvas.scrollHeight);

var pos = { x: 0, y: 0 };

canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mousedown', setPosition);
canvas.addEventListener('mouseenter', setPosition);
canvas.addEventListener('mouseup', historyUpdateBrush);

function setPosition(e) {
  pos.x = e.offsetX;
  pos.y = e.offsetY;
}

function historyUpdateBrush() {
  if (selectedTool == 'brush' || selectedTool == 'eraser') {
    updateHistory();
  }
  return;
}

function resize(w, h) {
  ctx.canvas.width = w
  ctx.canvas.height = h

  ctx2.canvas.width = w
  ctx2.canvas.height = h

}

function draw(e) {
  if (e.buttons !== 1) return;
  if (!drawing) return;
  if ((selectedTool !== "brush") && (selectedTool !== "eraser")) return;

  if (selectedTool === "brush") {
    ctx.globalCompositeOperation = "source-over";
  }
  if (selectedTool === "eraser") {
    ctx.globalCompositeOperation = "destination-out";
  }

  ctx.beginPath();
  ctx.globalAlpha = penOpacity;

  ctx.lineWidth = Math.pow(penSize, 2) / 100;
  ctx.lineCap = 'round';
  ctx.strokeStyle = selectedColor

  ctx.moveTo(pos.x, pos.y);
  setPosition(e);
  ctx.lineTo(pos.x, pos.y);

  ctx.stroke();
  ctx.globalCompositeOperation = "source-over";

}




//----------------------------------------------------------------------
// Make draggable:

const dragL = document.getElementById('draggableLeft');
const dragR = document.getElementById('draggableRight');
const menuL = document.getElementById('left');
const menuR = document.getElementById('right');

var wasDrawing = false;

dragElement(dragL, menuL);
dragElement(dragR, menuR);

function dragElement(elmnt, sticky) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  elmnt.onmousedown = dragMouseDown;
  function dragMouseDown(e) {

    if (drawing) {
      wasDrawing = true;
    }
    drawing = false

    e = e || window.event;
    e.preventDefault();
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    sticky.style.top = (elmnt.offsetTop - pos2 + 25) + "px";
    sticky.style.left = (elmnt.offsetLeft - pos1) + "px";
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    document.onmouseup = null;
    document.onmousemove = null;
    if (wasDrawing) {
      drawing = true;
    }
  }
}


//----------------------------------------------------------------------
// Download Function

function download() {
  var link = document.createElement('a');
  link.download = 'my_drawing.png';
  link.href = canvas.toDataURL()
  link.click();
}

function upload() {
  let u = document.getElementById('upload').files[0];
  let img = new Image();
  const reader = new FileReader();

  reader.addEventListener("load", function () {
    // convert image file to base64 string
    img.src = reader.result;
    img.onload = function () {
      canvas.style.width = img.naturalWidth + "px";
      canvas.style.height = img.naturalHeight + "px";
      previewCanvas.style.width = img.naturalWidth + "px";
      previewCanvas.style.height = img.naturalHeight + "px";
      resize(img.naturalWidth, img.naturalHeight);
      ctx.drawImage(img, 0, 0);
      updateHistory();
    }
  }, false);

  if (u) {
    reader.readAsDataURL(u);
  }


}



//----------------------------------------------------------------------
// Toggle Menu

function toggleMenu(tool) {

  function toggleSubMenuView(tool) {

    let toggleable = new RegExp("shape|text|file|color");

    resetAllButtons(tool);

    if (tool !== "color") {
      let m = document.getElementById(tool);
      m.style.backgroundColor = "rgb(160, 182, 255)";
    }


    if (toggleable.test(tool)) {
      let m = document.getElementById(tool + "Menu");
      if (selectedTool === tool) {
        m.style.display = "none";
        resetAllButtons();
        selectedTool = null;
      } else {
        closeAllSubMenu();
        m.style.display = "flex";
        selectedTool = tool;
      }
    } else {
      closeAllSubMenu();
      selectedTool = tool;
      switch (tool) {
        case "brush":
          drawing = true;
          document.getElementById('myCanvas').style.cursor = "url('./assets/brush.png') 0 64, default";
          return;
        case "eraser":
          drawing = true;
          document.getElementById('myCanvas').style.cursor = "url('./assets/eraser.png') 0 64, default";
          return;
        case "circle":
          document.getElementById('myCanvas').style.cursor = "url('./assets/circleCursor.png') 0 64, default";
          return;
        case "triangle":
          document.getElementById('myCanvas').style.cursor = "url('./assets/triangleCursor.png') 0 64, default";
          return;
        case "rectangle":
          document.getElementById('myCanvas').style.cursor = "url('./assets/rectCursor.png') 0 64, default";
          return;
        case "star":
          document.getElementById('myCanvas').style.cursor = "url('./assets/starCursor.png') 0 64, default";
          return;
        case "delete":
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          ctx2.clearRect(0, 0, canvas.width, canvas.height);
          resize(800, 600);
          resetAllButtons();
          updateHistory();
          return;
        default: return;
      }
    }

    return;
  }

  function closeAllSubMenu() {
    document.getElementById("shapeMenu").style.display = "none";
    document.getElementById("textMenu").style.display = "none";
    document.getElementById("fileMenu").style.display = "none";
    document.getElementById("colorMenu").style.display = "none";
    closeTextBox();
  }

  function resetAllButtons() {
    document.getElementById('brush').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('eraser').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('shape').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('text').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('file').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('undo').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('redo').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('delete').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('circle').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('triangle').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('star').style.backgroundColor = "rgb(146, 146, 146)";
    document.getElementById('rectangle').style.backgroundColor = "rgb(146, 146, 146)";
    closeTextBox();
    document.getElementById('myCanvas').style.cursor = "default";

  }

  toggleSubMenuView(tool);
}




//----------------------------------------------------------------------
// Color Menu

var colorCanvas = document.getElementById('colorpicker');
var ctxColor = colorCanvas.getContext('2d');

var colorPickerImage = new Image(100, 100);
colorPickerImage.onload = () => {
  ctxColor.drawImage(colorPickerImage, 0, 0, colorCanvas.width, colorCanvas.height);
}
colorPickerImage.setAttribute('crossOrigin', '');
colorPickerImage.src = "./assets/ColorWheel.png";

ctxColor.canvas.width = 100;
ctxColor.canvas.height = 100;

colorCanvas.onclick = function (e) {
  var imgData = ctxColor.getImageData(e.offsetX, e.offsetY, 1, 1);
  console.log(e.offsetX + ", " + e.offsetY)
  console.log(ctxColor.canvas.width);

  ctxColor.fillStyle = "blue";

  var rgba = imgData.data;
  console.log(rgba);
  var color = "rgb(" + rgba[0] + ", " + rgba[1] + ", " + rgba[2] + ")"
  document.getElementById("colorMenu").style.backgroundColor = color;
  document.getElementById("color").style.backgroundColor = color;
  selectedColor = color;
}

function changeBrightness(val) {
  console.log('brightness(' + val + '%)');
  ctxColor.filter = 'brightness(' + val + '%)';
  ctxColor.drawImage(colorPickerImage, 0, 0, colorCanvas.width, colorCanvas.height);

}
//----------------------------------------------------------------------
// Slider Menu

var opaSlider = document.getElementById('opaSlid');
var opaDisp = document.getElementById('showOpa');
var opaPreview = document.getElementById('opaPrev');

opaSlider.oninput = function () {
  penOpacity = this.value / 100;
  opaDisp.style.display = "flex";
  opaPreview.style.backgroundColor = "rgba(255, 255, 255, " + penOpacity + ")"
}

opaSlider.addEventListener('mouseup', function () {
  opaDisp.style.display = "none";
})


var penSizeSlider = document.getElementById('sizeSlid');
var sizeDisp = document.getElementById('showSize');
var penPreview = document.getElementById('penPrev');

penSizeSlider.oninput = function () {
  penSize = this.value;
  sizeDisp.style.display = "flex";

  penPreview.style.width = Math.pow(this.value, 2) / 100 + "px";
  penPreview.style.height = Math.pow(this.value, 2) / 100 + "px";
}
penSizeSlider.addEventListener('mouseup', function () {
  sizeDisp.style.display = "none";
})

//----------------------------------------------------------------------
// upload image

let uploadPlace = document.getElementById('upload');
uploadPlace.addEventListener("change", uploadedFile, false);

var upIMG;

function uploadedFile(e) {
  let f = new FileReader();
  f.readAsDataURL(e.target.files[0]);
  let img = f.result;
  let resultIMG = new Image();
  resultIMG.onload = function () {
    ctx.drawImage(resultIMG, 0, 0);
  };
  resultIMG.src = img;
}


//----------------------------------------------------------------------
//draw shape

var shapeInit = { x: 0, y: 0 };
var shapeDelta = { x: 0, y: 0 };
var isDrawingShape = false;

previewCanvas.addEventListener('mousemove', drawShape);
canvas.addEventListener('mousedown', initDrawShape);
previewCanvas.addEventListener('mouseup', finishDrawShape);
previewCanvas.addEventListener('mouseleave', finishDrawShape);

function drawShape(e) {
  if (!isDrawingShape) return;
  if (selectedTool !== "circle" && selectedTool !== "triangle" && selectedTool !== "rectangle" && selectedTool !== "star") return;

  shapeDelta = { x: e.offsetX - shapeInit.x, y: e.offsetY - shapeInit.y };
  ctx2.clearRect(0, 0, previewCanvas.width, previewCanvas.height);
  ctx2.drawImage(oriImage, 0, 0);
  ctx2.beginPath();
  ctx2.globalAlpha = penOpacity;
  ctx2.lineWidth = Math.pow(penSize, 2) / 100;
  ctx2.lineCap = 'round';
  ctx2.strokeStyle = selectedColor
  if (selectedTool === "circle") {
    circle(shapeInit, shapeDelta, ctx2);
  } else if (selectedTool === "rectangle") {
    rectangle(shapeInit, shapeDelta, ctx2);
  } else if (selectedTool === "triangle") {
    triangle(shapeInit, shapeDelta, ctx2);
  } else if (selectedTool === "star") {
    star(shapeInit, shapeDelta, ctx2);
  }
  ctx2.stroke();
}

function initDrawShape(e) {
  if (selectedTool !== "circle" && selectedTool !== "triangle" && selectedTool !== "rectangle" && selectedTool !== "star") return;

  ctx2.drawImage(oriImage, 0, 0);

  shapeInit.x = e.offsetX
  shapeInit.y = e.offsetY

  previewCanvas.style.display = 'flex';
  previewCanvas.style.position = 'fixed';

  isDrawingShape = true;
}

function finishDrawShape(e) {
  if (selectedTool !== "circle" && selectedTool !== "triangle" && selectedTool !== "rectangle" && selectedTool !== "star") return;
  if (!isDrawingShape) return;
  shapeDelta.x = e.offsetX - shapeInit.x
  shapeDelta.y = e.offsetY - shapeInit.y
  ctx.beginPath();
  ctx.globalAlpha = penOpacity;
  ctx.lineWidth = Math.pow(penSize, 2) / 100;
  ctx.lineCap = 'round';
  ctx.strokeStyle = selectedColor

  if (selectedTool === "circle") {
    circle(shapeInit, shapeDelta, ctx);
  } else if (selectedTool === "rectangle") {
    rectangle(shapeInit, shapeDelta, ctx);
  } else if (selectedTool === "triangle") {
    triangle(shapeInit, shapeDelta, ctx);
  } else if (selectedTool === "star") {
    star(shapeInit, shapeDelta, ctx);
  }
  ctx.stroke();
  updateHistory();
  isDrawingShape = false;
  previewCanvas.style.display = 'none'
}

//circle
function circle(start, delta, c) {
  c.arc(start.x + 0.5 * delta.x, start.y + 0.5 * delta.y, 0.5 * Math.sqrt(Math.pow(delta.x, 2) + Math.pow(delta.y, 2)), 0, 2 * Math.PI);
}

function rectangle(start, delta, c) {
  c.beginPath();
  c.moveTo(start.x, start.y);
  c.lineTo(start.x + delta.x, start.y);

  c.moveTo(start.x + delta.x, start.y);
  c.lineTo(start.x + delta.x, start.y + delta.y);

  c.moveTo(start.x + delta.x, start.y + delta.y);
  c.lineTo(start.x, start.y + delta.y);

  c.moveTo(start.x, start.y + delta.y);
  c.lineTo(start.x, start.y);
}

function triangle(start, delta, c) {
  ctx.lineCap = 'round';

  c.beginPath();
  c.moveTo(start.x + delta.x / 2, start.y);
  c.lineTo(start.x + delta.x, start.y + delta.y);

  c.moveTo(start.x + delta.x, start.y + delta.y);
  c.lineTo(start.x, start.y + delta.y);

  c.moveTo(start.x, start.y + delta.y);
  c.lineTo(start.x + delta.x / 2, start.y);
}

function star(start, delta, c) {
  let rot = Math.PI / 2 * 3;
  let step = Math.PI / 5;
  let x = start.x;
  let y = start.y;
  let outerRadius = Math.sqrt(Math.pow(delta.x, 2) + Math.pow(delta.y, 2))
  let innerRadius = 0.4 * outerRadius;

  c.save();
  c.translate(x, y)
  c.rotate(delta.x > 0 ? Math.asin(delta.y / outerRadius) : Math.acos(delta.y / outerRadius));
  c.translate(-1 * x, -1 * y)
  c.beginPath();
  ctx.moveTo(start.x, start.y - outerRadius);
  for (i = 0; i < 5; i++) {
    x = start.x + Math.cos(rot) * outerRadius;
    y = start.y + Math.sin(rot) * outerRadius;
    c.lineTo(x, y)
    c.moveTo(x, y)
    rot += step
    x = start.x + Math.cos(rot) * innerRadius;
    y = start.y + Math.sin(rot) * innerRadius;
    c.lineTo(x, y)
    c.moveTo(x, y)
    rot += step
  }
  c.lineTo(start.x, start.y - outerRadius);
  c.moveTo(start.x, start.y - outerRadius);
  c.closePath();
  c.restore();
}

//----------------------------------------------------------------------
//font stuff

var changingText = false;
var bold = false;
var italic = false;
var textPos = { x: 0, y: 0 };

window.onload = () => {
  document.getElementById('fontSizeNum').value = textSize;
  document.getElementById('textPreview').style.fontSize = textSize;
  changeTextBoxSize(textSize);
}
document.getElementById('fontSizeNum').onchange = () => {
  textSize = document.getElementById('fontSizeNum').value;
  changeTextBoxSize(textSize);
}
canvas.addEventListener("mouseup", setTextBox);

function setBold(elmnt) {
  if (bold) {
    bold = false;
    elmnt.style.backgroundColor = 'rgb(104, 103, 103)';
  } else {
    elmnt.style.backgroundColor = "rgb(160, 182, 255)"
    bold = true;
  }
}

function setItalic(elmnt) {
  if (italic) {
    italic = false;
    elmnt.style.backgroundColor = 'rgb(104, 103, 103)';
  } else {
    elmnt.style.backgroundColor = "rgb(160, 182, 255)"
    italic = true;
  }
}

function writeOnCanvas() {
  let txt = document.getElementById('textPreview').value;
  let fontString = (italic ? "italic " : "") + (bold ? "bold " : "") + textSize + "px" + ' ' + textFont;
  ctx.font = fontString;
  ctx.fillStyle = selectedColor
  console.log(selectedColor);
  textPos.y += 0.9 * parseInt(textSize);
  ctx.globalAlpha = penOpacity;
  ctx.fillText(txt, textPos.x, textPos.y);
  updateHistory();

}
function setTextBox(e) {
  if (!changingText) return;
  let inputSet = document.getElementById('inputSet');
  let textBox = document.getElementById('textPreview');

  textPos.x = e.offsetX;
  textPos.y = e.offsetY;


  textBox.style.font = textFont;
  let ws = (parseInt(canvas.width) - parseInt(e.offsetX));
  textBox.style.width = ws + 'px';

  inputSet.style.left = e.pageX + "px";
  inputSet.style.top = e.pageY + "px";
  inputSet.style.display = 'flex'
}

function changeTextBoxSize(h) {
  let textBox = document.getElementById('textPreview');
  textBox.style.height = (parseInt(h) + 10) + "px";
  textBox.style.fontSize = h + "px";
}

function textStyleButton() {
  if (changingText) {
    closeTextBox();
    closeFontChoice();
  } else {
    closeFontChoice();
    document.getElementById('myCanvas').style.cursor = "url('./assets/textCursor.png') 0 64, default";

    document.getElementById('fontSize').style.backgroundColor = "rgb(160, 182, 255)"
    document.getElementById('fontSizeSubMenu').style.display = 'flex';
    changingText = true;
  }

}
function closeTextBox() {
  document.getElementById('myCanvas').style.cursor = "default";

  document.getElementById('fontSize').style.backgroundColor = 'rgb(146, 146, 146)';
  document.getElementById('fontSizeSubMenu').style.display = 'none';
  changingText = false;
  let inputSet = document.getElementById('inputSet');
  inputSet.style.display = 'none';
}

var changingFont = false;


function fontButton() {
  if (changingFont) {
    closeFontChoice();
    closeTextBox();
  } else {
    closeTextBox();
    document.getElementById('fonts').style.backgroundColor = "rgb(160, 182, 255)"
    document.getElementById('fontsMenu').style.display = 'flex';
    changingFont = true;
  }
}

function closeFontChoice() {
  document.getElementById('fonts').style.backgroundColor = 'rgb(146, 146, 146)';
  document.getElementById('fontsMenu').style.display = 'none';
  changingFont = false;
}

function changeFont(o) {
  textFont = o.value;
}


//----------------------------------------------------------------------
//save, undo, redo stuff


var undo = [];
var redo = []

//call whenever make changes to canvas

document.getElementById('right').onload = updateHistory();
function updateHistory() {
  oriCanvasState = canvas.toDataURL();
  oriImage.src = oriCanvasState;

  undo.push(oriCanvasState);
  if (undo.length > 11) {
    undo.shift();
  }
  redo = [];
  document.getElementById('undoNum').innerHTML = undo.length - 1;
  document.getElementById('redoNum').innerHTML = redo.length;
}

function undoButtonClicked() {
  if (undo.length < 1) {
    console.log(redo.length);
    return;
  }
  redo.push(undo[undo.length - 1]);
  undo.pop();

  let i = new Image();
  i.src = undo[undo.length - 1]

  i.onload = function () {
    canvas.style.width = i.naturalWidth + "px";
    canvas.style.height = i.naturalHeight + "px";
    previewCanvas.style.width = i.naturalWidth + "px";
    previewCanvas.style.height = i.naturalHeight + "px";
    resize(i.width, i.height);

    ctx2.clearRect(0, 0, previewCanvas.width, previewCanvas.height);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(i, 0, 0);
    oriImage = i;
    document.getElementById('undoNum').innerHTML = undo.length - 1;
    document.getElementById('redoNum').innerHTML = redo.length;

  }
}

function redoButtonClicked() {
  if (redo.length < 1) return;

  let i = new Image();
  i.src = redo[redo.length - 1]

  i.onload = function () {

    canvas.style.width = i.naturalWidth + "px";
    canvas.style.height = i.naturalHeight + "px";
    previewCanvas.style.width = i.naturalWidth + "px";
    previewCanvas.style.height = i.naturalHeight + "px";
    resize(i.width, i.height);

    undo.push(redo[redo.length - 1]);
    redo.pop();

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(i, 0, 0);
    document.getElementById('undoNum').innerHTML = undo.length - 1;
    document.getElementById('redoNum').innerHTML = redo.length;

  }
}